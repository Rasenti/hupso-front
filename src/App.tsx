import { useState, useEffect } from 'react'
import './App.css'

function App() {

    interface Book {
        id: number;
        title: string;
        author: string;
        category: string;
        description: string;
    }

    const [books, setBooks] = useState<Book[]>([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);

    const getRandomImageUrl = () => {
        const randomId = Math.floor(Math.random() * 100);
        return `https://picsum.photos/id/${randomId}/200/200`;
    };

    useEffect(() => {
        const fetchBooks = async () => {
            try {
                const response = await fetch('http://localhost:8000/api/books', {
                    headers: {
                        'Accept': 'application/json'
                    }
                });

                if (!response.ok) {
                    throw new Error('Erreur lors de la récupération des livres');
                }

                const data = await response.json();

                setBooks(data);
            } catch (error) {
                console.log('test');
                setError(error.message);
            } finally {
                setLoading(false);
            }
        };

        fetchBooks();
    }, []);

    if (loading) {
        return <div>Chargement...</div>;
    }

    if (error) {
        return <div>Erreur : {error}</div>;
    }

    return (
        <div className="p-6">
            <h1 className="text-2xl font-bold mb-10">Bibliothèque</h1>

            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
                {books.map((book) => (
                    <div key={book.id} className="bg-white rounded-3xl shadow-md overflow-hidden flex flex-col justify-between h-full">
                        <div className="h-100 bg-gray-200">
                            <img
                                src={getRandomImageUrl()}
                                alt={`Couverture de ${book.title}`}
                                className="w-full h-full object-cover"
                            />
                        </div>

                        <div className="p-4 text-left">
                            <h2 className="text-xl font-semibold mb-2 truncate">{book.title} - {book.author}</h2>
                            <div className="text-gray-700 mb-2 bg-blue-200 text-center py-1 px-3 rounded-lg inline-block">{book.category}</div>
                            <p className="text-black line-clamp-3">{book.description}</p>
                        </div>
                        <div className="p-4 text-right">
                            <button
                                className="bg-black hover:bg-gray-800 text-white font-bold py-2 px-4 rounded-lg w-full">
                            Louer
                            </button>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    )
}

export default App
